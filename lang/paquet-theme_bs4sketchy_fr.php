<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bs4simplex
// Langue: fr
// Date: 17-04-2020 16:50:04
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4sketchy_description' => 'A hand-drawn look for mockups and mirth',
	'theme_bs4sketchy_slogan' => 'A hand-drawn look for mockups and mirth',
);
?>